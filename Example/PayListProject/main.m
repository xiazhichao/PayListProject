//
//  main.m
//  PayListProject
//
//  Created by “xzc” on 09/03/2018.
//  Copyright (c) 2018 “xzc”. All rights reserved.
//

@import UIKit;
#import "PayListProjectAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PayListProjectAppDelegate class]));
    }
}
