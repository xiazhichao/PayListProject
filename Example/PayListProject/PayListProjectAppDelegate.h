//
//  PayListProjectAppDelegate.h
//  PayListProject
//
//  Created by “xzc” on 09/03/2018.
//  Copyright (c) 2018 “xzc”. All rights reserved.
//

@import UIKit;

@interface PayListProjectAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
