# PayListProject

[![CI Status](https://img.shields.io/travis/“xzc”/PayListProject.svg?style=flat)](https://travis-ci.org/“xzc”/PayListProject)
[![Version](https://img.shields.io/cocoapods/v/PayListProject.svg?style=flat)](https://cocoapods.org/pods/PayListProject)
[![License](https://img.shields.io/cocoapods/l/PayListProject.svg?style=flat)](https://cocoapods.org/pods/PayListProject)
[![Platform](https://img.shields.io/cocoapods/p/PayListProject.svg?style=flat)](https://cocoapods.org/pods/PayListProject)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PayListProject is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'PayListProject'
```

## Author

“xzc”, “1256511693@qq.com”

## License

PayListProject is available under the MIT license. See the LICENSE file for more info.
